#!/bin/bash


# install pyenv
pyenv --version > /dev/null
if [ $? -ne 0 ]; then
    read -p "'pyenv' is not install, do you want install it? (Y/n) " choice
    if [[ $choice == "y" || $choice == "Y" || -z $choice ]]; then
        echo "Well ok I will try to take care of it but I promise nothing!"
    else
        echo "You are free to install it manually!"
        exit 1
    fi

    # get dependencies
    which apt > /dev/null
    if [ $? -ne 0 ]; then
        which yum > /dev/null
        if [ $? -ne 0 ]; then
            echo "Sorry, I can't do it! https://devguide.python.org/setup/#build-dependencies"
            exit 1
        else
            sudo yum install zlib-devel bzip2 bzip2-devel readline-devel sqlite \
                sqlite-devel openssl-devel xz xz-devel libffi-devel gcc make
        fi
    else
        sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
            libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
            libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl
    fi
    if [ $? -ne 0 ]; then
        echo "Sorry, I can't do it! https://devguide.python.org/setup/#build-dependencies"
        exit 1
    fi

    # install pyenv
    curl https://pyenv.run | bash
    if [ $? -ne 0 ]; then
        echo "Sorry, I can't do it!"
        exit 1
    fi
    echo ""
    echo "# >>> pyenv initialize >>>" >> ~/.bashrc
    echo "export PYENV_ROOT=\"$HOME/.pyenv\"" >> ~/.bashrc
    echo "export PIPENV_PYTHON=\"$PYENV_ROOT/shims/python\"" >> ~/.bashrc
    echo "export PATH=\"$HOME/.pyenv/bin:$PATH\"" >> ~/.bashrc
    echo "eval \"$(pyenv init -)\"" >> ~/.bashrc
    echo "eval \"$(pyenv virtualenv-init -)\"" >> ~/.bashrc
    echo "# <<< pyenv initialize <<<" >> ~/.bashrc

    exec $SHELL
fi

# install all python version
for version in '3.10-dev' '3.9-dev' '3.8-dev'
do
    pyenv local $version
    if [ $? -ne 0 ]; then
        read -p "'python$version' is not install, do you want install it? (Y/n) " choice
        if [[ $choice == "y" || $choice == "Y" || -z $choice ]]; then
            pyenv install -v $version
        else
            echo "You are free to install it manually!"
            exit 1
        fi
    fi
    pyenv local $version
    python -m pip install -U pip
    python -m pip install -U networkx
    python -m pip install -U pytest
done

pyenv local system
python -m raisin set verbosity 0

# test for each version
clear
for version in '3.11' '3.10' '3.9' '3.8'
do
    pyenv local $version-dev

    python -m pytest --full-trace --doctest-modules context_verbose/
    if [ $? -ne 0 ]; then
        exit 1
    fi
done

exit 0
